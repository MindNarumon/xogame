import java.util.Scanner;
public class Game {
	public Game() {
		
		x = new Player(name:'X');
		o = new Player(name:'O');
	}
	static int winner=0,player=0,go,row,column,line;
	
	static char [][] Table = {
			{'1','2','3'},
			{'4','5','6'},
			{'7','8','9'}};	
		
		
		static void showWelcome() {
			System.out.println("Welcome to Game XO");
		}

		static void showTable() {
			
				System.out.println("\n");
				System.out.printf("\t\t\t\t %c | %c | %c\n",Table[0][0],Table[0][1],Table[0][2]);
				System.out.print ("\t\t\t\t---+---+---\n");
				System.out.printf("\t\t\t\t %c | %c | %c\n",Table[1][0],Table[1][1],Table[1][2]);
				System.out.print ("\t\t\t\t---+---+---\n");
				System.out.printf("\t\t\t\t %c | %c | %c\n",Table[2][0],Table[2][1],Table[2][2]);
				
	  }		

		static void input() {
			Scanner kb= new Scanner(System.in);
			
			do {
				System.out.println("");
				System.out.printf("\n\t\t Player %d, please enter the number [your] %c : ",player,(player==1)?'X':'O');
				
				go =kb.nextInt();
				if(!(go>=1 && go<=9)) {
					System.out.print("!!! ERROR !!! please enter the number");
					System.out.printf("\n\t\t Player %d, please enter the number [your] %c : ",player,(player==1)?'X':'O');
					go =kb.nextInt();
				}
				row = --go/3;
				column = go%3;
			}while(go<0 || go>9 || Table[row][column] > '9');
		
			Table[row][column] = (player ==1) ? 'X':'O';
				
		}
	
	
	
	
	
	
	

}
