
public class Player {

	char name;
	int win;
	int draw;
	int lose;
	
	public Player(char name) {
		this.name = name;
	}
	public char getName() {
		return name;
	}
	public int getWin() {
		return win;
	}
	public int getDraw() {
		return draw;
	}
	public int getLose() {
		return lose;
	}
	public void win() {
		win++;
	}
	public void draw() {
		draw++;
	}
	public void lose() {
		lose++;
	}
}
